﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
//string bufResponse = httpSlayer.MakeRequest(UrlNum, enco: encoding, Headers: Headers, TimeOut: TimeOut);
//httpSlayer.dispose();



public class HTTPSlayer
{
    private Socket sock { get; set; }

    private string ipProxy { get; set; }
    private int portProxy { get; set; }
    private int TimeOut;
    public HTTPSlayer(string ipSocks, int portSocks, string toHostN, int TimeOut = 30000)
    {
        this.TimeOut = TimeOut;
        ipProxy = ipSocks;
        portProxy = portSocks;
        generateSocket(ipProxy, portProxy, toHostN);
    }

    private void generateSocket(string ipSocks, int portSocks, string toHostN)
    {
        
     
            try
            {
                SocksProxy socksProxy = new SocksProxy();
                sock = socksProxy.ConnectToSocks4Proxy(ipSocks, (ushort)portSocks, toHostN, (ushort)80, TimeOut);
            }
            catch (Exception e) { }
     
        if (sock == null)
        {
            try
            {
                SocksProxy socksProxy = new SocksProxy();
                sock = socksProxy.ConnectToSocks5Proxy(ipSocks, (ushort)portSocks, toHostN, (ushort)80, "", "", TimeOut);
            }
            catch (Exception e) { }
        }
    }
    public string MakeRequest(string toHostN, Encoding enco = null, List<Tuple<string, string>> Headers = null)
    {
        try
        {
            if (sock.Connected)
            {
                if (enco == null)
                    enco = Encoding.UTF8;

                string query = GetHttpText(new Uri(toHostN), GetHeaderStr(Headers));

                byte[] buf = Encoding.UTF8.GetBytes(query);
                sock.Send(buf, buf.Length, 0);

                //Application.DoEvents();

                byte[] readBuf = new byte[1024];
                int resC = 0;
                string resp = "";
                while ((resC = sock.Receive(readBuf, readBuf.Length, 0)) > 0)
                {
                    resp += enco.GetString(readBuf, 0, resC);
                }
                return RedirectTry(resp, enco, Headers);
            }
        }
        catch (Exception e)
        { }
        return "1";
    }
    public string GetHttpText(Uri ToHOst, string Headers)
    {
        return "GET " + ToHOst.PathAndQuery + " HTTP/1.1\r\n" +
                                  "Host:" + ToHOst.Host + "\r\n" +
                                  "Connection: keep-alive\r\n" +
                                  "User-Agent: Mozilla/5.0 (Windows NT 5.1; rv:10.0.2) Gecko/20100101 Firefox/10.0.2\r\n" +
                                  Headers +
                                  "\r\n";
    }
    public string GetHeaderStr(List<Tuple<string, string>> Headers)
    {
        var StrHeadres = "";
        if (Headers != null)
            Headers.ForEach(x => StrHeadres += x.Item1 + ": " + x.Item2 + "\r\n");
        return StrHeadres;
    }

    public void dispose()
    {
        sock.Disconnect(false);
        sock.Dispose();
        sock = null;
    }

    public string RedirectTry(string resp, Encoding enco = null, List<Tuple<string, string>> Headers = null)
    {
        var arrayRes = resp.Split(new[] { '\r', '\n' });
        if (arrayRes[0].Contains("301 Moved"))
        {
            return "1";
            var RedirectURL = arrayRes.Where(x => x.Contains("Location:")).First().Replace("Location:", "").Trim();
            dispose();
            generateSocket(ipProxy, portProxy, RedirectURL);

            string RespAfterRedirect = MakeRequest(RedirectURL, enco, Headers);


        }

        return resp;
    }
}

