﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using xNet;
using System.Windows.Forms;

/// <summary>
/// Сводное описание для HttpInteractMitSBIS
/// </summary>
public class MyWebClient : WebClient
{
    public int TimeOut { get; set; }
    protected override WebRequest GetWebRequest(Uri uri)
    {
        WebRequest w = base.GetWebRequest(uri);
        if (TimeOut == 0)
            TimeOut = 15000;
        w.Timeout = TimeOut;
        return w;
    }
}

public class HttpInteract
{
    public static string CreatePost(string url, List<Tuple<string, string>> ListParam, string json)
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        request.Method = "POST";
        request.ContentType = "application/x-www-form-urlencoded";//!very  important
        request.Host = (new Uri(url)).Host;
        request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
        using (var streamWriter = new StreamWriter(request.GetRequestStream()))
        {
            streamWriter.Write(MakePostFields(ListParam));
            streamWriter.Write(json);
            streamWriter.Flush();
            streamWriter.Close();
        }
        HttpWebResponse response = null;
        try
        {
            response = (HttpWebResponse)request.GetResponse();
        }
        catch (WebException ex)
        {
            response = ex.Response as HttpWebResponse;
        }
        string contentType = response.ContentType;
        Stream content = response.GetResponseStream();
        StreamReader contentReader = new StreamReader(content, Encoding.UTF8);
        string resStr = contentReader.ReadToEnd();
        return resStr;
    }

    public static string MakePostFields(List<Tuple<string, string>> ListParam)
    {
        return string.Join("&", ListParam.Select(c => c.Item1 + "=" + c.Item2).ToArray());
    }
    public static string RetriveValueFromResLvlONE(string paramName, string result)
    {
        //Документ.Идентификатор
        var dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(result);
        string ResStr = "";
        if (dict[paramName] is System.Collections.Generic.Dictionary<string, object> || dict[paramName] is System.Collections.ArrayList)
            ResStr = new JavaScriptSerializer().Serialize(dict[paramName]);
        else
            ResStr = dict[paramName].ToString();
        return ResStr;
    }
    public static string GetPageWebBrowser(string url)
    {
        var webBrowser = new WebBrowser();
        webBrowser.Navigate(url);
        return webBrowser.DocumentText;
    }
    public static string GetPage(string url, Encoding enco = null, CookieContainer listCookies = null, WebProxy proxy = null,
        List<Tuple<string, string>> Headers = null, int TimeOut = 30000, string method = "GET", List<Tuple<string, string>> fields = null)
    {
        try
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = method;
            request.Timeout = TimeOut;

            if (proxy != null)
                request.Proxy = proxy;

            request.UserAgent = "Mozilla/5.0 (Windows NT 5.1; rv:10.0.2) Gecko/20100101 Firefox/10.0.2";
            if (listCookies != null)
            {
                request.CookieContainer = listCookies;
            }
            if (Headers != null)
                foreach (var ItemHeader in Headers)
                {
                    if (!SetProperty(request, ItemHeader.Item1, ItemHeader.Item2))
                        request.Headers.Add(ItemHeader.Item1, ItemHeader.Item2);
                }
            if (fields != null)
                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(MakePostFields(fields));
                    streamWriter.Flush();
                    streamWriter.Close();
                }
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                return RetriveResponse(response, enco);
            }

        }
        catch (WebException ex)
        {
            if (ex.Message.Contains("503"))
            {
                return RetriveResponse((HttpWebResponse)ex.Response, enco);
            }
        }
        catch (Exception ex) { }

        return "1";
    }
    private static string RetriveResponse(HttpWebResponse response, Encoding enco)
    {
        var encoding = ASCIIEncoding.UTF8;
        if (enco != null)
            encoding = enco;
        using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encoding))
        {
            string responseText = reader.ReadToEnd();
            return responseText;
        }
    }
    public static string GetPage_new(string url, CookieContainer listCookies = null, Encoding enco = null, WebProxy proxy = null, List<Tuple<string, string>> Headers = null, int TimeOut = 30000)
    {
        if (enco == null)
            enco = ASCIIEncoding.UTF8;

        using (var request = new HttpRequest())
        {
            request.UserAgent = Http.ChromeUserAgent();
            request.CharacterSet = enco;
            request.ConnectTimeout = TimeOut;

            if (proxy != null)
            {
                var xNetproxy = TryCreateProxy(proxy.Address);
                request.Proxy = xNetproxy;
            }

            if (listCookies != null)
                request.Cookies = RetriveDictionaryCookies(listCookies, url);

            if (Headers != null)
                foreach (var ItemHeader in Headers)
                {
                    if (!SetProperty(request, ItemHeader.Item1, ItemHeader.Item2))
                        request.AddHeader(ItemHeader.Item1, ItemHeader.Item2);
                }

            string content = request.Get(url).ToString();
            SetContainerCookies(request.Cookies, url, listCookies);
            return content;
        }
    }

    private static CookieDictionary RetriveDictionaryCookies(CookieContainer listCookies, string url)
    {
        url = "http://" + new Uri(url).Host;
        var uri = new Uri(url);
        var cookieDict = new CookieDictionary();

        foreach (Cookie cookie in listCookies.GetCookies(uri))
        {
            if (!cookieDict.ContainsKey(cookie.Name))
                cookieDict.Add(cookie.Name, cookie.Value);
            else
                cookieDict[cookie.Name] = cookie.Value;
        }

        return cookieDict;
    }
    private static void SetContainerCookies(CookieDictionary ListCookies, string url, CookieContainer containerCookies)
    {
        if (containerCookies != null)
        {
            url = "http://" + new Uri(url).Host;
            var uri = new Uri(url);
            if (ListCookies != null)
                foreach (var itemDIct in ListCookies)
                {
                    containerCookies.Add(uri, new Cookie(itemDIct.Key, itemDIct.Value));
                }
        }
    }
    private static ProxyClient TryCreateProxy(Uri Address)
    {
        ProxyClient proxy = null;
        try
        {
            proxy = HttpProxyClient.Parse(Address.Host + ":" + Address.Port);
        }
        catch { }
        try
        {
            if (proxy == null)
                proxy = Socks4ProxyClient.Parse(Address.Host + ":" + Address.Port);
        }
        catch { }
        try
        {
            if (proxy == null)
                proxy = Socks5ProxyClient.Parse(Address.Host + ":" + Address.Port);
        }
        catch { }
        return proxy;
    }

    private static bool SetProperty(object Obj, string PropName, string PropValue)
    {
        bool isOk = false;
        try
        {
            var TypeObj = Obj.GetType();
            var Prop = TypeObj.GetProperty(PropName);
            Prop.SetValue(Obj, PropValue);
            isOk = true;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        return isOk;
    }


}