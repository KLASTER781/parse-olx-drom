﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
using System.Threading;

namespace CallOfAvito
{
    public partial class Form1 : Form
    {
        int i = 0;
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void mouse_event(uint dwFlags, uint dx, uint dy, uint cButtons, uint dwExtraInfo);
        //Mouse actions
        private const int MOUSEEVENTF_LEFTDOWN = 0x02;
        private const int MOUSEEVENTF_LEFTUP = 0x04;
        private const int MOUSEEVENTF_RIGHTDOWN = 0x08;
        private const int MOUSEEVENTF_RIGHTUP = 0x10;

        public Action<string> ProcessingPhone { get; set; }

        public ChromiumWebBrowser browser;

        public Form1()
        {
            InitializeComponent();
            InitBrowser();
            //timer1.Start();
        }
        public void InitBrowser()
        {
            Cef.Initialize(new CefSettings());
            browser = new ChromiumWebBrowser("www.google.com");
            this.Controls.Add(browser);
            browser.Dock = DockStyle.Fill;

        }

        private void webcontrolEventListenerTextLoad(object sender, FrameLoadEndEventArgs e)
        {
            if (e.Frame.IsMain)
            {
                browser.FrameLoadEnd -= webcontrolEventListenerTextLoad;
                var tr = new Task(() =>
                {


                    browser.GetSourceAsync().ContinueWith(taskHtml =>
                    {
                        var html = taskHtml.Result;
                        if (SkanAvito.aweProcess.SetText != null)
                            SkanAvito.aweProcess.SetText.Invoke(html);

                        SkanAvito.aweProcess.SetText = null;
                    });
                });
                tr.Start();
            }
        }


        public void doSomeScrap(string url)
        {
            browser.FrameLoadEnd += webcontrolEventListener2;
            browser.Load(url);
        }


        private void webcontrolEventListener2(object sender, FrameLoadEndEventArgs e)
        {
            browser.FrameLoadEnd -= webcontrolEventListener2;
            var tr = new Task(() =>
            {
                Thread.Sleep(2000);
                clickByClass("'#changeLang'", x: 3, y: 27);

                Thread.Sleep(2000);
                clickByClass("'.olx-rebranded-welcome__close-btn'", x: 40, y: 35);
                Thread.Sleep(2000);
                clickByClass("'.cookiesBarClose'", x: 5, y: 25);
                Thread.Sleep(2000);
                clickByClass(x: 150, y: 47);//, 188, 47);
                Thread.Sleep(2000);

                try
                {
                    var myScript = @"(function () {
                                return $('strong.xx-large').first().text();
                                })();";

                    browser.EvaluateScriptAsync(myScript).ContinueWith(t =>
                    {
                        var result = t.Result;
                        if (result != null && result.Result != null)
                        {

                            string phone = result.Result.ToString();
                            Console.WriteLine(phone);

                            Thread.Sleep(2000);
                            clickByClass("'#changeLang'", x: 3, y: 27);
                            ProcessingPhone.Invoke(phone);
                        }
                    });
                    
                }
                catch { }

            });
            tr.Start();
        }


        protected void clickByClass(string jqSelector = "'div.link-phone'", int x = 0, int y = 0)
        {
            var myScript = @"(function () {
                                return $(" + jqSelector + @").first().offset().left  +' '+ $(" + jqSelector + @").first().offset().top;
                                })();";

            browser.EvaluateScriptAsync(myScript).ContinueWith(t =>
            {
                var result = t.Result;
                if (result != null && result.Result != null)
                {
                    string left = result.Result.ToString().Split(' ')[0].Replace(".", ",");

                    string top = result.Result.ToString().Split(' ')[1].Replace(".", ",");
                    clickBtn((int)decimal.Parse(left) + x, (int)decimal.Parse(top) + y);
                }
            });
        }
        protected void clickByTitle(string itemTitle, string tag = "button", Action afterLoad = null, int iterat = 1, int offsetForBottom = 0)
        {
            browser.ExecuteScriptAsync(@"function getPos(el) {
                    for (var lx = 0, ly = 0;
                        el != null;
                        lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent) ;
                        return { x: lx,y: ly}; 
                    }");

            var myScript = @"(function () {
            var iterat = " + iterat + @";
            var aTags = document.getElementsByTagName(""" + tag + @""");
            var searchText = """ + itemTitle + @""";
            var found;

            for (var i = 0; i < aTags.length; i++)
            {
                if (aTags[i].textContent == searchText )
                    {         
                    
                        if( iterat == 1)
                            {
                                found = aTags[i];
                                break;
                            }
                        iterat--;
                    }
            }
                                return getPos(found).x +' '+ getPos(found).y;
                                })();";

            browser.EvaluateScriptAsync(myScript).ContinueWith(t =>
            {
                var result = t.Result;
                if (result != null && result.Result != null)
                {
                    string left = result.Result.ToString().Split(' ')[0];
                    string top = result.Result.ToString().Split(' ')[1];
                    clickBtn(int.Parse(left) + 20, int.Parse(top) + 25);
                    Thread.Sleep(1000);
                    if (offsetForBottom > 0)
                    {
                        clickBtn(int.Parse(left) + 20, int.Parse(top) + 25 + offsetForBottom);

                    }
                    if (afterLoad != null)
                        afterLoad.Invoke();


                }
            });
        }
        protected void clickByTitleJS(string itemTitle, string tag = "button")
        {
            browser.ExecuteScriptAsync(@"function eventFire(el, etype){
                                         if (el.fireEvent) {
                                         el.fireEvent('on' + etype);
                                            } else {
                                         var evObj = document.createEvent('Events');
                                         evObj.initEvent(etype, true, false);
                                         el.dispatchEvent(evObj);
                                         }
                                        }
            
            var aTags = document.getElementsByTagName(""" + tag + @""");
            var searchText = """ + itemTitle + @""";
            var found;
            
            for (var i = 0; i < aTags.length; i++)
            {
                if (aTags[i].textContent == searchText )
                    {         
                                found = aTags[i];
                                break;
                    }
            }
            
            eventFire(found,'click');
            
                                        
");


        }
        protected void clickById(string idElement, Action afterLoad = null)
        {
            browser.ExecuteScriptAsync(@"function getPos(el) {
                    for (var lx = 0, ly = 0;
                        el != null;
                        lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent) ;
                        return { x: lx,y: ly}; 
                    }");

            var myScript = @"(function () {
                                return getPos(document.getElementById('" + idElement + @"')).x;
                                })();";

            browser.EvaluateScriptAsync(myScript).ContinueWith(t =>
            {
                var result = t.Result;
                if (result != null && result.Result != null)
                {
                    string left = result.Result.ToString();
                    myScript = @"(function () {
                                return getPos(document.getElementById('" + idElement + @"')).y;
                                })();";
                    browser.EvaluateScriptAsync(myScript).ContinueWith(t2 =>
                    {
                        var resultSUB = t2.Result;
                        if (resultSUB != null && resultSUB.Result != null)
                        {
                            string top = resultSUB.Result.ToString();
                            clickBtn(int.Parse(left) + 20, int.Parse(top) + 65);
                            if (afterLoad != null)
                                afterLoad.Invoke();
                        }
                    });
                }
            });
        }

        protected void clickBtn(int x = 1332, int y = 321)
        {
            this.BeginInvoke((MethodInvoker)(() =>
            {
                this.Cursor = new Cursor(Cursor.Current.Handle);
                Cursor.Position = new Point(x, y);
                mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, (uint)x, (uint)y, 0, 0);
            }));
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (SkanAvito.aweProcess.makeRequest != null)
            {

                var url = SkanAvito.aweProcess.makeRequest.Invoke();
                SkanAvito.aweProcess.makeRequest = null;

                if (SkanAvito.aweProcess.SetText != null)
                {
                    browser.FrameLoadEnd += webcontrolEventListenerTextLoad;
                    browser.Load(url);
                }
                else
                {
                    doSomeScrap(url);
                    ProcessingPhone = (x) =>
                    {
                        SkanAvito.aweProcess.lastExe = DateTime.Now;

                        if (SkanAvito.aweProcess.SetPhone != null)
                        {
                            SkanAvito.aweProcess.SetPhone.Invoke(x);
                            SkanAvito.aweProcess.SetPhone = null;
                        }

                    };
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (SkanAvito.aweProcess.makeRequest != null)
            {
                var url = SkanAvito.aweProcess.makeRequest.Invoke();
                doSomeScrap(url);
                ProcessingPhone = (x) =>
                {
                    if (SkanAvito.aweProcess.SetPhone != null)
                    {
                        SkanAvito.aweProcess.SetPhone.Invoke(x);
                    }

                };
            }
        }

        public Action ClosFrm
        {
            get
            {
                return () => { this.Close(); };
            }
        }
    }
}
