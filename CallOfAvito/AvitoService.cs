﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceProcess;
using System.Threading;
using System.Net;

namespace CallOfAvito
{
    class AvitoService : ServiceBase
    {
        public AvitoService()
        {
            this.ServiceName = "CallOfOLX";
            this.CanStop = true;
            this.CanPauseAndContinue = false;
            this.AutoLog = true;
        }
        protected override void OnStart(string[] args)
        {
            // TODO: add startup stuff
            var seekerPage = new SeekerPage();


            Thread tSeeker = new Thread(new ParameterizedThreadStart(seekerPage.seek));
            tSeeker.Start(true);

            SkanAvito skanAvito = new SkanAvito();
            
            Thread t = new Thread(new ThreadStart(skanAvito.StartCycle));
            
            t.Start();

        }
       
        protected override void OnStop()
        {
            SkanAvito.Finish = true;
            // TODO: add shutdown stuff
        }

    }
}
