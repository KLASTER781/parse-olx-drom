﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CallOfAvito
{
    class SeekerPage
    {
        PageAvito ThreeAvito = new PageAvito(SkanAvitoThread.url_site, int.MaxValue);
        CookieContainer cookies;
        public SeekerPage()
        {


        }

        public void seek(object IsAddParty)
        {
            ThreeAvito.children.Add(new PageAvito("https://www.olx.ua/list/", int.MaxValue));

            foreach (PageAvito ItemPage in ThreeAvito.children)
                SubSeek(ItemPage);

            while (true)
            {
                if (CheckAllComplited())
                {
                    CallResetAllStep();
                    break;
                }
                Thread.Sleep(3600000);
            }
            this.seek(!(bool)IsAddParty);
        }
        private void AddOtherParty(bool IsAddParty)
        {
            string query = "";
            try
            {
                query = ConfigurationManager.AppSettings.Get("QueryStrForSkan");
            }
            catch { }
            if (IsAddParty && query != "")
                cookies = SkanAvito.GetCookies(SkanAvitoThread.url_site + "?" + query);
            else
                cookies = new CookieContainer();



        }
        private void seekForCicle()
        {

        }
        public void SubSeek(PageAvito ItemPage)
        {
            Thread.Sleep(5000);

            string page = HttpInteract.GetPage(ItemPage.link, listCookies: cookies);

           //<a[^>]*topLink tdnone[^>]*href=\"([^\"]*)\"[^>]*>[^<]*<[^>]*><[^>]*>[^<]*<[^>]*><[^>]*>[^<]*<[^>]*>[^<]*<[^>]*>([^<]*)

            string regularEx = "<a[^>]*topLink tdnone[^>]*href=\\\"([^\"]*)\\\"[^>]*>[^<]*<[^>]*><[^>]*>[^<]*<[^>]*><[^>]*>[^<]*<[^>]*>[^<]*<[^>]*>([^<]*)";
            if (page.IndexOf("Крупнейшие города") >= 0)
                page = page.Substring(1, page.IndexOf("Крупнейшие города") + 1);

            foreach (List<string> itemHref in RetriveRecords(page, regularEx))
            {
                Uri myUri = new Uri( itemHref.First());
                ItemPage.children.Add(new PageAvito(SkanAvitoThread.url_site + myUri.AbsolutePath + "?view=list", int.Parse(itemHref[1].Replace(" ", ""))));
            }

            if (ItemPage.children.Count == 0)
                StoreToBase(ItemPage);
            else

                foreach (PageAvito subPage in ItemPage.children)

                    if (subPage.sizePage > 10000)
                        SubSeek(subPage);
                    else
                        StoreToBase(subPage);


        }

        public static List<List<string>> RetriveRecords(string page, string PatternRec)
        {

            var ListRecord = new List<List<string>>();
            Regex rgx = new Regex(PatternRec, RegexOptions.IgnoreCase);
            MatchCollection matches = rgx.Matches(page);
            foreach (Match itemRec in matches)
            {
                List<string> subList = new List<string>();
                foreach (Group itemRes in itemRec.Groups.Cast<Group>().ToList().Skip(1))
                    subList.Add(itemRes.Value);
                ListRecord.Add(subList);
            }

            return ListRecord;
        }
        public static Match RetriveMatch(string page, string PatternRec)
        {
            Regex rgx = new Regex(PatternRec, RegexOptions.IgnoreCase);
            var match = rgx.Match(page);
            return match;
        }
        public void StoreToBase(PageAvito ItemPage)
        {
            try
            {
                using (var context = new masterEntities())
                {
                    string cityName = new Uri(ItemPage.link).AbsolutePath;
                    var NewitemStep = (from o in context.itemStep
                                       where o.SityName == cityName
                                       select o).FirstOrDefault();
                    if (NewitemStep == null)
                    {


                        NewitemStep = new itemStep();
                        NewitemStep.LastStep = (ItemPage.sizePage / 100) + 1;
                        NewitemStep.step = 1;
                        NewitemStep.Priority = 1;
                        NewitemStep.SityName = new Uri(ItemPage.link).AbsolutePath;
                        context.itemStep.Add(NewitemStep);
                        context.SaveChanges();
                    }
                    NewitemStep.LastStep = (ItemPage.sizePage / 100) + 1;
                    NewitemStep.LastStep = (NewitemStep.LastStep > 100 ? 100 : NewitemStep.LastStep);
                    context.Entry(NewitemStep).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();
                }
            }
            catch { }
        }
        public bool CheckAllComplited()
        {
            using (var context = new masterEntities())
            {
                int count = (from o in context.itemStep
                             where o.step <= o.LastStep
                             select o).Count();
                if (count > 0)
                    return false;
            }
            return true;
        }
        public void CallResetAllStep()
        {
            using (var context = new masterEntities())
            {
                var result = (from o in context.itemStep
                              select o).ToList();
                result.ForEach((x) => x.step = 1);
                context.SaveChanges();

            }
        }

    }
    class PageAvito
    {
        public string link { get; set; }
        public int sizePage { get; set; }
        public List<PageAvito> children { get; set; }
        public PageAvito(string url, int sizePage)
        {
            this.sizePage = sizePage;
            link = url;
            children = new List<PageAvito>();
        }
    }

}
