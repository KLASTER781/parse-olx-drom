﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CallOfAvito
{
    class Program
    {
        static void Main()
        {
            //System.ServiceProcess.ServiceBase.Run(new AvitoService());

            var seekerPage = new SeekerPage();
            Thread tSeeker = new Thread(new ParameterizedThreadStart(seekerPage.seek));
            tSeeker.Start(true);

            SkanAvito skanAvito = new SkanAvito();
            Thread t = new Thread(new ThreadStart(skanAvito.StartCycle));
            t.Start();

            Console.ReadLine();
            SkanAvito.Finish = true;
        }
    }
}
