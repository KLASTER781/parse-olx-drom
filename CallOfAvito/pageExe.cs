﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallOfAvito
{
    public class pageExe
    {
        public pageExe(string href = "", string subway = "", string type = "", int city = 1)
        {
            this.href = href;
            this.subway = subway;
            this.type = type;
            this.city = city;
        }
        public string href { get; set; }
        public string subway { get; set; }
        public string type { get; set; }
        public string number { get; set; }
        public string NameSellaer { get; set; }
        public int city { get; set; }
        public string textPage { get; set; }
        public string TitlePage { get; set; }
        public string DatePublic { get; set; }
        public List<List<string>> CategoryIerarhy { get; set; }
    }
}
