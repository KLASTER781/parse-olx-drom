﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CallOfAvito.AppCode
{
    public class AwesomProcess
    {
        public Form1 frm;
        public void RunProcess()
        {
            frm = new Form1();
            Application.Run(frm);
        }
        public void StopProcess()
        {
            frm.ClosFrm.Invoke();
            
            Application.ExitThread();
        }

        public Func<string> makeRequest { get; set; }
        public Action<string> SetPhone { get; set; }
        public Action<string> SetText { get; set; }
        public DateTime lastExe = DateTime.Now;
    }
}
