﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Specialized;
using MyProxyList;
using CallOfAvito.AppCode;

namespace CallOfAvito
{
    class SkanAvito
    {

        public static List<pageExe> ListRecord = new List<pageExe>();
        public List<SkanAvitoThread> PoolSkanAvito = new List<SkanAvitoThread>();
        public static AwesomProcess aweProcess;
        public static bool Finish = false;
        public static HidMeProxy MyProxy = new HidMeProxy();
        private static int city = 1;
        public IPAddress[] IPSiteForSocks4 { get; set; }
        private int i;
        private string urlPage;

        private delegate void ForThread(string urlPage);
        private void rewriteDigitalPhone()
        {
            using (var context = new masterEntities())
            {
                while (true)
                {
                    var ListPhone = (from o in context.sms_phone
                                     where o.DigitalPhone == null
                                     select o).Take(1000);
                    if (ListPhone.Count() == 0)
                        break;
                    foreach (sms_phone itemPhone in ListPhone)
                    {
                        itemPhone.DigitalPhone = SkanAvitoThread.RetriveDigitFromPhone(itemPhone.number);
                    }
                    context.SaveChanges();
                }
            }
        }
        public void StartCycle()
        {
            MyProxy.checkProxyIsSet += new Func<WebProxy, bool>(x => PoolSkanAvito.Where(y => y.numberRecognize.MyProxy != null && y.numberRecognize.MyProxy.Address.Host == x.Address.Host).Count() > 0);

            Thread.Sleep(int.Parse(ConfigurationManager.AppSettings.Get("Sleep")));

            UpdateTask();
            int countThread = 50;
            try
            {
                countThread = int.Parse(ConfigurationManager.AppSettings.Get("CountThread"));
            }
            catch { }
            rewriteDigitalPhone();

            create_ThreadsPool(countThread);

            string page = "";
            while ((page = HttpInteract.GetPage(urlPage.ToString() + "&page=" + i++)) != "" && !Finish)
            {
                if (page.Contains("IP-адреса временно"))
                {
                    Thread.Sleep(300000);
                }
                else
                {
                    setItemStep(i);

                    ListRecord = RetriveRecords(page);


                    while (ListRecord.Count > 0 && !Finish)
                    {
                        CreateBrowserthread();
                        Thread.Sleep(1);
                    }

                    if (getSizeRAM() > 500)
                        Environment.Exit(1);

                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
            }
            Environment.Exit(1);

        }
        public void CreateBrowserthread()
        {
            if (aweProcess == null)
            {
                aweProcess = new AwesomProcess();
                Thread t = new Thread(new ThreadStart(aweProcess.RunProcess));
                t.SetApartmentState(ApartmentState.STA);
                t.Start();
            }

            if (aweProcess.lastExe.AddSeconds(600) < DateTime.Now)
            {
                //Перезапуск
                
                aweProcess.StopProcess();
                aweProcess = new AwesomProcess();
                Thread t = new Thread(new ThreadStart(aweProcess.RunProcess));
                t.SetApartmentState(ApartmentState.STA);
                t.Start();
            }
        }
        private void create_ThreadsPool(int count)
        {


            for (; 0 < count; count--)
            {
                var itemObjThread = new SkanAvitoThread();
                itemObjThread.who = this;
                PoolSkanAvito.Add(itemObjThread);

                Thread t = new Thread(new ThreadStart(itemObjThread.MethodForPool));
                t.Start();
            }
        }
        public void CreateAwesom()
        {

        }
        public static CookieContainer GetCookies(string url)
        {
            try
            {
                //lock(this)
                //proxy
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.UserAgent = "Mozilla/5.0 (Windows NT 5.1; rv:10.0.2) Gecko/20100101 Firefox/10.0.2";
                request.CookieContainer = new CookieContainer();
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    return request.CookieContainer;
                }
            }
            catch { }
            return new CookieContainer();
        }
        private List<pageExe> RetriveRecords(string page)
        {
            string PatternRec
        = "<a[^>]*?href=\\\"([^>]*?)\\\"[^>]*?class=\\\"[^>]*?linkWithHash[^>]*?detailsLink\\\"[^>]*?>";
            //"class=\"data.*\r?\n.*\r?\n?([^<>]*)<?/?p?>?\r?\n?.*\r?\n.*<a class=\"description-title-link\"[^>]*href=\"([^\"]*)\".*\r?\n.*\r?\n[^>]*>([^<]*)";
            var ListRecord = new List<pageExe>();
            Regex rgx = new Regex(PatternRec, RegexOptions.IgnoreCase);
            MatchCollection matches = rgx.Matches(page);
            foreach (Match itemRec in matches)
            {
                ListRecord.Add(new pageExe(href: itemRec.Groups[1].Value, subway: "", type: "", city: city));
            }

            return ListRecord;
        }

        ///store item step
        public static int itemIDStep { get; set; }
        private static int retriveItemIndex()
        {
            try
            {
                using (var context = new masterEntities())
                {
                    itemStep itemSteRec = context.itemStep.Find(itemIDStep);
                    if (itemSteRec == null)
                    {
                        itemSteRec = new itemStep();
                        itemSteRec.step = itemIDStep;
                        context.itemStep.Add(itemSteRec);
                        context.SaveChanges();
                    }
                    return (int)itemSteRec.step;
                }
            }
            catch { }
            return 1;
        }
        public void setItemStep(int index)
        {
            try
            {
                using (var context = new masterEntities())
                {
                    itemStep itemSteRec = context.itemStep.Find(itemIDStep);
                    if (itemSteRec == null)
                    {
                        itemSteRec = new itemStep();
                        itemSteRec.step = itemIDStep;
                        context.itemStep.Add(itemSteRec);
                        context.SaveChanges();
                    }
                    if (index >= 0)
                        itemSteRec.step = index;

                    itemSteRec.DateExecute = DateTime.Now;
                    context.SaveChanges();

                    if (itemSteRec.step > itemSteRec.LastStep)
                    {
                        //change item whay
                        UpdateTask();
                    }
                }
            }
            catch { }

        }
        public static int FindCreateCity(string city_name)
        {
            try
            {
                using (var contextMy = new masterEntities())
                {


                    var City = (from o in contextMy.sms_city
                                where o.city_name == city_name
                                select o).FirstOrDefault();
                    if (City == null)
                    {
                        City = new sms_city();
                        City.city_name = city_name;

                        contextMy.sms_city.Add(City);
                        contextMy.SaveChanges();
                    }
                    return City.id;
                }
            }
            catch { }
            return 1;

        }

        protected void UpdateTask()
        {
            itemStep ItemSt = GetItemTask();
            SkanAvito.itemIDStep = ItemSt.id;
            urlPage = SkanAvitoThread.url_site + ItemSt.SityName + "?bt=0&view=list";
            i = retriveItemIndex();



            city = FindCreateCity(new Uri(urlPage).AbsolutePath);
        }
        protected itemStep GetItemTask()
        {
            itemStep itemSteRec = null;
            do
            {
                try
                {
                    using (var context = new masterEntities())
                    {
                        var CountSkiping = int.Parse(ConfigurationManager.AppSettings.Get("Skip"));
                        if (int.Parse(ConfigurationManager.AppSettings.Get("OrderBy")) == 1)
                            itemSteRec = (from o in context.itemStep
                                          where o.step <= o.LastStep
                                          select o).OrderByDescending(x => x.id).Skip(CountSkiping).FirstOrDefault();//
                        else
                            itemSteRec = (from o in context.itemStep
                                          where o.step <= o.LastStep
                                          select o).OrderBy(x => x.id).Skip(CountSkiping).FirstOrDefault();//


                    }
                }
                catch { Thread.Sleep(100); }
            } while (itemSteRec == null);
            return itemSteRec;
        }

        public static long getSizeRAM()
        {
            Process proc = Process.GetCurrentProcess();
            return (proc.PrivateMemorySize64) / 1024 / 1024;
        }
    }
}
