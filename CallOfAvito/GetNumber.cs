﻿using Microsoft.Win32;
using OCR.TesseractWrapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Net;
using System.Reflection;
using System.Security.Policy;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using tessnet2;


namespace CallOfAvito
{

    public class GetNumber
    {
        private WebProxy myProxy = null;
        public WebProxy MyProxy
        {
            get
            {
                if (int.Parse(ConfigurationManager.AppSettings.Get("UseProxy")) == 0)
                    return null;
                else
                    return myProxy;
            }
            set { myProxy = value; }
        }
        public string number { get; set; }
        public string NameSeller { get; set; }
        public List<List<string>> CategoryIerarhy { get; set; }
        public string textPage { get; set; }
        public string TitlePage { get; set; }
        public string DatePublic { get; set; }
        public object who { get; set; }
        public static TesseractProcessor processor { get; set; }
        public static int TimeOut
        {
            get
            {
                var _timeOut = 15000;
                try
                {
                    _timeOut = int.Parse(ConfigurationManager.AppSettings.Get("TimeOut"));
                }
                catch { }
                return _timeOut;
            }
        }


        private CookieContainer listCookies = new CookieContainer();

        public void GetNumber_method(string url)
        {
            url = TrimUrl(url);
            listCookies = new CookieContainer();
            var phoneLock = ParsePage(url);

            if (!phoneLock)
            {
                lock (who)
                {
                    number = GetFromBrowser(url, "");// MyProxy.Address.Host + ":" + MyProxy.Address.Port);
                }
            }
        }

        public bool ParsePage(string urlPage)
        {
            string downloadString = "";
            lock (who)
            {
               downloadString = GetTextFromBrowser(urlPage);  // HttpInteract.GetPage(urlPage,  proxy: MyProxy, TimeOut: TimeOut);
            }
            string Patern_Name = "offer-user__actions[^>]*>[^<]*<[^>]*>[^<]*<[^>]*>([^<]*)<";
            string Pattern_Category = "class=\"inline[^>]*>[\\s\\S]*?<a[^>]*href=\"([^>]*)\"[^>]*><span[^>]*>([^<]*)<";
            string Pattern_TextPage = "textContent[^>]*>([\\s\\S]*?)</div";
            string Pattern_TitlePage = ">([^>]*)</h1>";
            string Pattern_DatePublic = "Добавлено:[^<]*<[^>]*>[^<]*<[^>]*>([^<]*)<";
            string Pattern_DatePublic_next = "Опубликовано с мобильного</a>[^<]*<[^>]*>[^<]*<[^>]*>([^<]*)<";
            string Pattern_HavPhone = "<span>Показать</span>";

            NameSeller = SeekerPage.RetriveMatch(downloadString, Patern_Name).Groups[1].Value;

            textPage = WebUtility.HtmlEncode(SeekerPage.RetriveMatch(downloadString, Pattern_TextPage).Groups[1].Value);

            TitlePage = WebUtility.HtmlEncode(SeekerPage.RetriveMatch(downloadString, Pattern_TitlePage).Groups[1].Value);

            if (downloadString == "" || downloadString == "1")
                throw new Exception("Empty page from Http slayer");

            var RegRepl = new Regex("[\\s]{2,}");
            DatePublic = SeekerPage.RetriveMatch(downloadString, Pattern_DatePublic).Groups[1].Value;
            DatePublic = RegRepl.Replace(DatePublic, " ");
            var next_DatePublic = SeekerPage.RetriveMatch(downloadString, Pattern_DatePublic_next).Groups[1].Value;
            DatePublic += RegRepl.Replace(next_DatePublic, " ");

            CategoryIerarhy = SeekerPage.RetriveRecords(downloadString, Pattern_Category);
            if (downloadString.Contains("вашего IP-адреса"))
                return true;
            try
            {
                var MatchPhoneHave = SeekerPage.RetriveMatch(downloadString, Pattern_HavPhone);
                if (MatchPhoneHave.Groups[0].Value == "")
                    return true;
            }
            catch
            {

            }
            Thread.Sleep(500);
            return false;
        }
        public GetNumber(object who)
        {
            this.who = who;

        }
        private string TrimUrl(string dirtUrl)
        {
            var trimReg = new Regex(@"[\?|#].*");
            return trimReg.Replace(dirtUrl, "");
        }
        private string GetFromBrowser(string url, string proxy)
        {
            DropProxy();
            var phone = "";
            var tcs = new TaskCompletionSource<int>();
            SkanAvito.aweProcess.makeRequest += () =>
            {
                return url;
            };
            SkanAvito.aweProcess.SetPhone += (x) =>
            {
                phone = x;
                tcs.SetResult(1);
            };
            var res = tcs.Task.Result;

            DropProxy();
            return phone;
        }
        private string GetTextFromBrowser(string url)
        {
            DropProxy();
            var text = "";
            var tcs = new TaskCompletionSource<int>();
            SkanAvito.aweProcess.makeRequest += () =>
            {
                return url;
            };
            SkanAvito.aweProcess.SetText += (x) =>
            {
                text = x;
                tcs.SetResult(1);
            };
            var res = tcs.Task.Result;

            DropProxy();
            return text;
        }
        
        private void SetProxy(string Proxy)
        {
            string key = "Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings";
            RegistryKey RegKey = Registry.CurrentUser.OpenSubKey(key, true);
            RegKey.SetValue("ProxyServer", Proxy);
            RegKey.SetValue("ProxyEnable", 1);

        }
        private void DropProxy()
        {
            string key = "Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings";
            RegistryKey RegKey = Registry.CurrentUser.OpenSubKey(key, true);
            RegKey.SetValue("ProxyServer", "");
            RegKey.SetValue("ProxyEnable", 0);

        }
    }
}
