﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CallOfAvito
{
    public class SkanAvitoThread
    {
        public static string url_site = "https://www.olx.ua";
        public static string url_site_for_replace = "//www.olx.ua";
        public object who { get; set; }
        public GetNumber numberRecognize { get; set; }
        public void MethodForPool()
        {
            pageExe list = null;
            SkanAvito.MyProxy.who = who;
            numberRecognize = new GetNumber(who);
            numberRecognize.MyProxy = SkanAvito.MyProxy.GetNextProxy();
            while (!SkanAvito.Finish)
            {
                list = getNextPageExe();
                while (list != null)
                {
                    try
                    {
                        if (SkanAvito.Finish)
                            break;

                        numberRecognize.GetNumber_method(list.href);

                        list.number = numberRecognize.number;
                        list.NameSellaer = numberRecognize.NameSeller;
                        list.CategoryIerarhy = numberRecognize.CategoryIerarhy;
                        list.textPage = numberRecognize.textPage;
                        list.TitlePage = numberRecognize.TitlePage;
                        list.DatePublic = numberRecognize.DatePublic;
                        SaveRecToBaase(list);
                        list = getNextPageExe();
                        Thread.Sleep(5000);
                    }
                    catch (Exception e)
                    { numberRecognize.MyProxy = SkanAvito.MyProxy.GetNextProxy(); }
                    // Thread.Sleep(5000);

                }
                Thread.Sleep(1);
            }
        }
        private pageExe getNextPageExe()
        {
            pageExe ItemList = null;
            lock (who)
            {
                ItemList = SkanAvito.ListRecord.FirstOrDefault();
                if (SkanAvito.ListRecord.Count > 0)
                {
                    SkanAvito.ListRecord.RemoveAt(0);
                }
            }
            return ItemList;

        }

        ///Save
        private void SaveRecToBaase(pageExe rec)
        {
            lock (who)
            {
                int phoneRecId = FindCreatePhone(rec.number, rec.NameSellaer);
                TryAddrecForTypePhone(phoneRecId, rec.type);
                TryAddRecForSubWayPhone(phoneRecId, rec.subway);
                TryAddrecForCityPhone(phoneRecId, rec.city);
                TryAddrecForCategoryPhone(phoneRecId, rec.CategoryIerarhy);
                TryAddHrefForPhone(phoneRecId, rec.href, rec.textPage, rec.TitlePage, rec.DatePublic);
                ((SkanAvito)who).setItemStep(-1);
            }
        }
        private int FindCreatePhone(string phoneNumber, string NameSellaer)
        {
            try
            {
                var DigitalPhone = RetriveDigitFromPhone(phoneNumber);
                using (var contextMy = new masterEntities())
                {


                    var shcet = (from o in contextMy.sms_phone
                                 where o.DigitalPhone == DigitalPhone
                                 select o).FirstOrDefault();
                    if (shcet == null)
                    {
                        shcet = new sms_phone();
                        shcet.number = phoneNumber;

                        contextMy.sms_phone.Add(shcet);
                        contextMy.SaveChanges();
                    }
                    shcet.NameSellaer = NameSellaer;
                    shcet.DigitalPhone = RetriveDigitFromPhone(phoneNumber);
                    shcet.write = null;
                    contextMy.Entry(shcet).State = System.Data.Entity.EntityState.Modified;
                    contextMy.SaveChanges();
                    return shcet.id;
                }
            }
            catch { }
            return 1;

        }
        public static Int64 RetriveDigitFromPhone(string phone)
        {
            try
            {
                Regex OnlyDig = new Regex("[^0-9]*");
                phone = OnlyDig.Replace(phone, "");
                if (phone.Substring(0, 3) == "380")
                    phone = string.Join("", phone.Take(12));
                else
                    phone = string.Join("", phone.Take(10));
                Int64 DigitPhone = 0;
                Int64.TryParse(phone, out DigitPhone);
                return DigitPhone;
            }
            catch { }
            return 0;

        }
        private void TryAddrecForTypePhone(int idPhone, string type)
        {
            try
            {
                using (var context = new masterEntities())
                {
                    var sms_sectionRec = (from o in context.sms_section
                                          where o.name_section == type
                                          select o).FirstOrDefault();
                    if (sms_sectionRec == null)
                    {
                        sms_sectionRec = new sms_section();
                        sms_sectionRec.name_section = type;
                        context.sms_section.Add(sms_sectionRec);
                        context.SaveChanges();
                    }
                    int sectionId = sms_sectionRec.id;
                    var phoneToSectionRec = (from o in context.sms_phone_to_section
                                             where o.id_phone == idPhone && o.id_section == sectionId
                                             select o).FirstOrDefault();
                    if (phoneToSectionRec == null)
                    {
                        phoneToSectionRec = new sms_phone_to_section();
                        phoneToSectionRec.id_phone = idPhone;
                        phoneToSectionRec.id_section = sectionId;
                        context.sms_phone_to_section.Add(phoneToSectionRec);
                        context.SaveChanges();
                    }

                }
            }
            catch { }
        }
        private void TryAddRecForSubWayPhone(int idPhone, string subWay)
        {
            try
            {
                using (var context = new masterEntities())
                {
                    var subwayRec = (from o in context.sms_subway
                                     where o.name_stantion == subWay
                                     select o).FirstOrDefault();
                    if (subwayRec == null)
                    {
                        subwayRec = new sms_subway();
                        subwayRec.name_stantion = subWay;
                        context.sms_subway.Add(subwayRec);
                        context.SaveChanges();
                    }
                    int subwayId = subwayRec.id;

                    var phone_to_subwayRec = (from o in context.sms_phone_to_subway
                                              where o.id_phonne == idPhone && o.id_subway == subwayId
                                              select o).FirstOrDefault();
                    if (phone_to_subwayRec == null)
                    {
                        phone_to_subwayRec = new sms_phone_to_subway();
                        phone_to_subwayRec.id_phonne = idPhone;
                        phone_to_subwayRec.id_subway = subwayId;
                        context.sms_phone_to_subway.Add(phone_to_subwayRec);
                        context.SaveChanges();
                    }
                }
            }
            catch { }
        }

        private void TryAddrecForCityPhone(int idPhone, int idCity)
        {
            try
            {
                using (var context = new masterEntities())
                {
                    var phoneToCityRec = (from o in context.sms_phone_to_city
                                          where o.id_phone == idPhone && o.id_city == idCity
                                          select o).FirstOrDefault();
                    if (phoneToCityRec == null)
                    {
                        phoneToCityRec = new sms_phone_to_city();
                        phoneToCityRec.id_phone = idPhone;
                        phoneToCityRec.id_city = idCity;
                        context.sms_phone_to_city.Add(phoneToCityRec);
                        context.SaveChanges();
                    }

                }
            }
            catch { }
        }
        private void TryAddrecForCategoryPhone(int idPhone, List<List<string>> CategoryIerarhy)
        {
            int idCateg = GetCategoryByList(CategoryIerarhy);
            try
            {
                using (var context = new masterEntities())
                {
                    var phoneToCityRec = (from o in context.sms_phone_to_category
                                          where o.PhoneId == idPhone && o.CategoryId == idCateg
                                          select o).FirstOrDefault();
                    if (phoneToCityRec == null)
                    {
                        phoneToCityRec = new sms_phone_to_category();
                        phoneToCityRec.PhoneId = idPhone;
                        phoneToCityRec.CategoryId = idCateg;
                        context.sms_phone_to_category.Add(phoneToCityRec);
                        context.SaveChanges();
                    }

                }
            }
            catch { }
        }
        private void TryAddHrefForPhone(int idPhone, string href, string textPage, string TitlePage, string DatePublic)
        {
            try
            {
                using (var context = new masterEntities())
                {
                    var NewHref = new Links();
                    NewHref.Link = href;
                    NewHref.id_phone = idPhone;
                    NewHref.TextFromPage = textPage;
                    NewHref.TitlePage = TitlePage;
                    NewHref.DateLastVisit = DateTime.Now;
                    NewHref.DatePublic = DatePublic;
                    context.Links.Add(NewHref);
                    context.SaveChanges();
                }
            }
            catch { }
        }

        private int GetCategoryByList(List<List<string>> CategoryIerarhy)
        {
            try
            {
                using (var context = new masterEntities())
                {
                    int IdParentCateg = -1;
                    foreach (List<string> category in CategoryIerarhy)
                    {
                        string NameCateg = category[1];
                        string LinkCateg = category[0];
                        var CategoryInBase = (from o in context.Category
                                              where o.Link == LinkCateg
                                              select o).FirstOrDefault();

                        if (CategoryInBase == null)
                        {
                            CategoryInBase = new Category();
                            CategoryInBase.Name = NameCateg;
                            CategoryInBase.Link = LinkCateg;
                            CategoryInBase.Parent = IdParentCateg;
                            context.Category.Add(CategoryInBase);
                            context.SaveChanges();
                        }
                        if (CategoryInBase.Name.Length < NameCateg.Length)
                        {
                            CategoryInBase.Name = NameCateg;
                            context.Entry(CategoryInBase).State = System.Data.Entity.EntityState.Modified;
                            context.SaveChanges();
                        }
                        IdParentCateg = CategoryInBase.id;

                    }
                    return IdParentCateg;

                }
            }
            catch { }
            return 1;
        }

    }
}
