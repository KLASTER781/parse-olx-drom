﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RetriveReg
{
   public class RetriverReg
    {      
         public static List<List<string>> RetriveRecords(string page, string PatternRec)
        {
            var ListRecord = new List<List<string>>();
            Regex rgx = new Regex(PatternRec, RegexOptions.IgnoreCase);
            MatchCollection matches = rgx.Matches(page);
            foreach (Match itemRec in matches)
            {
                List<string> subList = new List<string>();
                foreach (Group itemRes in itemRec.Groups.Cast<Group>().ToList().Skip(1))
                    subList.Add(itemRes.Value);
                ListRecord.Add(subList);
            }
            return ListRecord;
        }
        public static Match RetriveMatch(string page, string PatternRec)
        {
            Regex rgx = new Regex(PatternRec, RegexOptions.IgnoreCase);
            var match = rgx.Match(page);
            return match;
        }
    }
}
