﻿using CsQuery;
using RetriveReg;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace MyProxyList
{


    public class proxyItem
    {
        public string ip { get; set; }
        public int port { get; set; }
        public proxyItem(string ip, int port)
        {
            this.ip = ip;
            this.port = port;
        }
    }
    public class HidMeProxy
    {
        private List<proxyItem> ListProxy = new List<proxyItem>();
        private int itemPageStartIndex = 0;
        private int AlternativeItemPageStartIndex = 1;
        public object who { get; set; }//******
        public string[] ListUrlProxySite
        {
            get
            {
                return GetUrlProxy();
            }
        }
        private CookieContainer listCookies = new CookieContainer();
        private void NextProxyList()
        {
            alternativeProxy();

            Thread.Sleep(5000);
            MatchCollection matches = null;
            itemPageStartIndex = 0;

            do
            {

                string url = "https://hidemy.name/proxy-list/?maxtime=3000&start=" + itemPageStartIndex + "#list";//&type=s45maxtime=3000&
                itemPageStartIndex += 64;
                string Page = BrowserCapcha(url);

                string patternProxy = "class=tdl>([^<]*)</td><td>([0-9]*)";
                Regex rgx = new Regex(patternProxy, RegexOptions.IgnoreCase);
                matches = rgx.Matches(Page);


                foreach (Match itemMatch in matches)
                    ListProxy.Add(new proxyItem(itemMatch.Groups[1].Value, int.Parse(itemMatch.Groups[2].Value)));
                Thread.Sleep(1000);
            } while (matches.Count > 0);


            SetSimpleProxy();
        }
        private string BrowserCapcha(string url)
        {

            string TextPage = HttpInteract.GetPage(url, Encoding.GetEncoding("windows-1251"), listCookies: listCookies);
            if (!TextPage.Contains("DDoS protection"))
                return TextPage;
            else
            {
                var domen = (new Uri(url)).Host;
                var Protocol = "https://";

                var fields = RetriveField(TextPage);
                fields.Add(RetriveCalculateField(TextPage, domen));
                var toGetTypeField = "?";
                toGetTypeField += String.Join("&", fields.Select(x => x.Item1 + "=" + x.Item2).ToArray());


                var headers = new List<Tuple<string, string>>();
                headers.Add(new Tuple<string, string>("Referer", url));
                Thread.Sleep(4000);
                return HttpInteract.GetPage(Protocol + domen + RetriveUrl(TextPage) + toGetTypeField,
                     Encoding.GetEncoding("windows-1251"), listCookies: listCookies
                   , Headers: headers);


            }
        }
        private List<Tuple<string, string>> RetriveField(string TextPage)
        {
            var ListField = new List<Tuple<string, string>>();
            ListField.AddRange(RetriverReg.RetriveRecords(TextPage, @"(?<=input).*name=""([^""]*)"" value=""([^""]*)").Select(x => new Tuple<string, string>(x[0], x[1])));
            return ListField;
        }
        private Tuple<string, string> RetriveCalculateField(string TextPage, string domen)
        {
            var sum = Calculate(RetriverReg.RetriveRecords(TextPage, @"\:([\+\(\)\!\[\]]*)\};")[0][0]);
            var FindedExpr = RetriverReg.RetriveRecords(TextPage, @"(?<=\.)[a-z]+([\+\-\*\/]{1})=([^;]+)");
            foreach (List<string> itemMatch in FindedExpr)
            {
                sum = CompliteSign(sum, Calculate(itemMatch[1]), itemMatch[0]);
            }
            return new Tuple<string, string>("jschl_answer", (sum + domen.Length).ToString());
        }
        private double Calculate(string dirtExpression)
        {
            EvalClass jScriptEvaluator = new EvalClass();
            var resultat = jScriptEvaluator.Evaluate(dirtExpression);
            return double.Parse(resultat.ToString());

        }
        private string RetriveUrl(string TextPage)
        {
            return RetriverReg.RetriveRecords(TextPage, @"(?<=action)=""([^""]*)""").FirstOrDefault()[0];
        }
        private double CompliteSign(double sum, double value, string sign)
        {
            if (sign == "*")
                sum *= value;
            if (sign == "/")
                sum /= value;
            if (sign == "-")
                sum -= value;
            if (sign == "+")
                sum += value;
            return sum;
        }
        public void alternativeProxy()
        {
            Thread.Sleep(500);
            string url = "https://free-proxy-list.net/anonymous-proxy.html";
            string Page = HttpInteract.GetPage(url);
            string patternProxy = @"<td>([0-9\.]*)</td><td>([0-9]*)</td>";
            Regex rgx = new Regex(patternProxy, RegexOptions.Singleline | RegexOptions.IgnoreCase);
            MatchCollection matches = rgx.Matches(Page);

            foreach (Match itemMatch in matches)
                ListProxy.Add(new proxyItem(itemMatch.Groups[1].Value, int.Parse(itemMatch.Groups[2].Value)));
        }
        private void SetSimpleProxy()
        {
            foreach (string url in ListUrlProxySite)
            {
                Thread.Sleep(500);
                MatchCollection matches = null;
                string Page = HttpInteract.GetPage(url);

                string patternProxy = "([0-9]+[\\.]+[0-9\\.]*)\\s*:\\s*([0-9]*)";
                Regex rgx = new Regex(patternProxy, RegexOptions.IgnoreCase);
                matches = rgx.Matches(Page);


                foreach (Match itemMatch in matches)
                    ListProxy.Add(new proxyItem(itemMatch.Groups[1].Value, int.Parse(itemMatch.Groups[2].Value)));
            }

        }
        private string TrimmEr(string source)
        {
            string result = "";
            // string PaternStyle = "<style>(.*?)</style>";
            string PaternElementStyle = "\\.([^\\{]*?)\\{([^\\}]*?)\\}";
            //string PaternIP = "<[span|div][^>\"]*?\"([^\"]*?)\">([0-9\\.]{1,3})";
            string PaternIP = "(\"[^\"]*?\")?>([0-9\\.\\s]*?)<";
            Dictionary<string, string> ReplaceMass = new Dictionary<string, string>();




            Regex rgx = new Regex(PaternElementStyle, RegexOptions.Singleline | RegexOptions.IgnoreCase);
            MatchCollection matches = rgx.Matches(source);

            foreach (Match itemMatch in matches)
                ReplaceMass.Add(itemMatch.Groups[1].Value, itemMatch.Groups[2].Value);

            rgx = new Regex(PaternIP, RegexOptions.Singleline | RegexOptions.IgnoreCase);
            matches = rgx.Matches(source);

            foreach (Match itemMatch in matches)
            {
                string itemPice = Replacer(itemMatch.Groups[1].Value.Replace("\"", ""), ReplaceMass);
                if (!itemPice.Contains("none"))
                    result += itemMatch.Groups[2].Value;
            }

            result = result.Replace(" ", "").Replace("\n", "");


            return result;
        }
        private string Replacer(string source, Dictionary<string, string> Dict)
        {
            if (Dict.ContainsKey(source))
                return Dict[source];
            return source;
        }
        private string RetriveIP(string html)
        {
            CQ cq = CQ.Create(html);
            foreach (IDomObject obj in cq.Find("h3.r a"))
                return obj.GetAttribute("href");
            return "";
        }
        public WebProxy GetNextProxy()
        {

            lock (who)
            {
                WebProxy proxy = null;
                do
                {
                    proxyItem result = null;
                    do
                    {
                        if (ListProxy.Count < 1)
                            NextProxyList();



                        result = ListProxy.FirstOrDefault();

                    } while (result == null);

                    if (ListProxy.Count > 0)
                        ListProxy.RemoveAt(0);

                    proxy = new WebProxy(result.ip, result.port);
                }
                while (checkProxyIsSet.Invoke(proxy));


                return proxy; //new WebProxy("127.0.0.1", 3128); // 
            }

        }////********/////*****
        public event Func<WebProxy, bool> checkProxyIsSet; //proxyTest.Address.Host//*/*/*/*/*/*/*/*/*/*/*/*
        private string[] GetUrlProxy()
        {
            return new string[] { };
            var CuntPRX = ListProxy.Count();
            while (CuntPRX >= 0)
            {
                var url = "http://www.google.ru/search?q=filetype:txt+%2B:80+%2B:8080+%2B:3128&newwindow=1&hl=ru&gbv=1&prmd=ivns&ei=DooRWOv0LsmpsgGVt4WYBw&start=0&sa=N";

                WebProxy _webProxy = null;
                if (CuntPRX == ListProxy.Count())
                    --CuntPRX;
                else
                {
                    var proxy = ListProxy[CuntPRX--];
                    _webProxy = new WebProxy(proxy.ip, proxy.port);
                }
                try
                {
                    var Page = HttpInteract.GetPage(url, proxy: _webProxy);
                    if (Page == "1")
                        throw new Exception("Error");
                    var PatternGoogleHref = "<cite>([\\s\\S]*?)</cite>";
                    Regex rgx = new Regex(PatternGoogleHref);
                    var matches = rgx.Matches(Page);
                    var ListUrl = new List<string>();
                    foreach (Match itemMatch in matches)
                        ListUrl.Add("http://" + itemMatch.Groups[1].Value);

                    return ListUrl.ToArray();
                }
                catch { }
            }
            return new string[] { };
        }


    }



}